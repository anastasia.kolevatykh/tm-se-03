package ru.kolevatykh.tm;

import ru.kolevatykh.tm.manager.TaskManager;

public class App {
    public static void main( String[] args ) {
        TaskManager taskManager = new TaskManager();
        taskManager.init();
    }
}
