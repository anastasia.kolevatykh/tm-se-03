package ru.kolevatykh.tm.manager;

import ru.kolevatykh.tm.constant.TerminalCommand;
import ru.kolevatykh.tm.service.ProjectService;
import ru.kolevatykh.tm.service.TaskService;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class TaskManager {
    private Scanner scanner = new Scanner(System.in);
    private ProjectService projectService = new ProjectService();
    private TaskService taskService = new TaskService();

    private static SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");

    private static Date parseDate(final String dateInput) {
        try {
            return dateFormat.parse(dateInput);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private void showCommands() {
        String help = TerminalCommand.HELP + ": \t\tShow all commands.\n"
                + TerminalCommand.PROJECT_CREATE + ": Create new project.\n"
                + TerminalCommand.PROJECT_LIST + ": \tShow all projects.\n"
                + TerminalCommand.PROJECT_SHOW + ": \tShow tasks of selected project.\n"
                + TerminalCommand.PROJECT_UPDATE + ": Update selected project.\n"
                + TerminalCommand.PROJECT_CLEAR + ": \tRemove all projects.\n"
                + TerminalCommand.PROJECT_REMOVE + ": Remove selected project.\n"
                + TerminalCommand.TASK_CREATE + ": \tCreate new task.\n"
                + TerminalCommand.TASK_LIST + ": \tShow all tasks.\n"
                + TerminalCommand.TASK_ASSIGN + ": \tAssign task to project.\n"
                + TerminalCommand.TASK_UPDATE + ": \tUpdate selected task.\n"
                + TerminalCommand.TASK_CLEAR + ": \tRemove all tasks.\n"
                + TerminalCommand.TASK_REMOVE + ": \tRemove selected task.\n"
                + TerminalCommand.EXIT + ": \t\tExit.";
        System.out.println(help);
    }

    private void createProject() {
        System.out.println("[PROJECT CREATE]\nEnter project name: ");
        String projectName = scanner.nextLine();

        System.out.println("Enter project description: ");
        String projectDescription = scanner.nextLine();

        System.out.println("Enter project start date: ");
        String projectStartDate = scanner.next();
        Date startDate = parseDate(projectStartDate);

        System.out.println("Enter project end date: ");
        String projectEndDate = scanner.next();
        Date endDate = parseDate(projectEndDate);

        projectService.create(projectName, projectDescription, startDate, endDate);
        System.out.println("[OK]");
    }

    private void showProjects() {
        System.out.println("[PROJECT LIST]");
        projectService.readAll();
    }

    private void showProjectTasks() {
        System.out.println("[PROJECT SHOW]");
        String projectId = findProjectByName();

        if (projectId != null) {
            System.out.println("[TASK LIST]");
            taskService.readByProjectId(projectId);
        }
    }

    private void updateProject() {
        String projectId = findProjectByName();

        System.out.println("Enter new project name: ");
        String projectName = scanner.nextLine();

        System.out.println("Enter new project description: ");
        String projectDescription = scanner.nextLine();

        projectService.update(projectId, projectName, projectDescription);
    }

    private void clearProjects() {
        taskService.clearAll();
        projectService.clearAll();
        System.out.println("[Removed all projects with tasks.]");
    }

    private void removeProjectByName() {
        System.out.println("[PROJECT REMOVE]\nEnter project name to remove: ");
        String projectName = scanner.nextLine();

        removeTaskByProjectId(projectName);
        projectService.removeByName(projectName);
        System.out.println("[Removed " + projectName + " project with tasks.]");
    }

    private String findProjectByName() {
        System.out.println("Enter project name: ");
        String projectName = scanner.nextLine();
        if (projectService.findByName(projectName) == null) {
            System.out.println("[The project '" + projectName + "' does not exist!]");
            return null;
        }
        return projectService.findByName(projectName).getId();
    }

    private void createTask() {
        System.out.println("[TASK CREATE]\nEnter task name: ");
        String taskName = scanner.nextLine();

        System.out.println("Enter task description: ");
        String taskDescription = scanner.nextLine();

        System.out.println("Enter task start date: ");
        String taskStartDate = scanner.next();
        Date startDate = parseDate(taskStartDate);

        System.out.println("Enter task end date: ");
        String taskEndDate = scanner.next();
        Date endDate = parseDate(taskEndDate);

        taskService.create(taskName, taskDescription, startDate, endDate);
        System.out.println("[OK]");
    }

    private void showTasks() {
        System.out.println("[TASK LIST]");
        taskService.readAll();
    }

    private void assignTaskToProject() {
        String taskId = findTaskByName();
        String projectId = findProjectByName();

        if (projectId != null) {
            taskService.assignToProject(taskId, projectId);
            System.out.println("[OK]");
        }
    }

    private void updateTask() {
        String taskId = findTaskByName();

        System.out.println("Enter new task name: ");
        String taskName = scanner.nextLine();

        System.out.println("Enter new task description: ");
        String taskDescription = scanner.nextLine();

        taskService.update(taskId, taskName, taskDescription);
    }

    private void clearTasks() {
        taskService.clearAll();
        System.out.println("[Removed all tasks.]");
    }

    private void removeTaskByProjectId(String projectName) {
        String projectId = projectService.findByName(projectName).getId();
        taskService.removeByProjectId(projectId);
    }

    private void removeTaskByName() {
        System.out.print("[TASK REMOVE]\nEnter project name: ");
        String projectName = scanner.nextLine();

        System.out.println("Enter task name:");
        String taskName = scanner.nextLine();

        String projectId = projectService.findByName(projectName).getId();
        taskService.removeByName(projectId, taskName);
    }

    private String findTaskByName() {
        System.out.println("Enter task name: ");
        String taskName = scanner.nextLine();
        if (taskService.findByName(taskName) == null) {
            System.out.println("[The project '" + taskName + "' does not exist!]");
            return null;
        }
        return taskService.findByName(taskName).getId();
    }

    public void init() {
        System.out.println("*** Welcome to task manager ***"
                + "\nType \"help\" for details.");

        String input;

        while (true) {
            input = scanner.nextLine();
            switch (input) {
                case TerminalCommand.HELP:
                case TerminalCommand.H:
                    showCommands();
                    break;
                case TerminalCommand.PROJECT_CREATE:
                case TerminalCommand.PCR:
                    createProject();
                    break;
                case TerminalCommand.PROJECT_LIST:
                case TerminalCommand.PL:
                    showProjects();
                    break;
                case TerminalCommand.PROJECT_SHOW:
                case TerminalCommand.PSH:
                    showProjectTasks();
                    break;
                case TerminalCommand.PROJECT_UPDATE:
                case TerminalCommand.PU:
                    updateProject();
                    break;
                case TerminalCommand.PROJECT_REMOVE:
                case TerminalCommand.PR:
                    removeProjectByName();
                    break;
                case TerminalCommand.PROJECT_CLEAR:
                case TerminalCommand.PCL:
                    clearProjects();
                    break;
                case TerminalCommand.TASK_CREATE:
                case TerminalCommand.TCR:
                    createTask();
                    break;
                case TerminalCommand.TASK_LIST:
                case TerminalCommand.TL:
                    showTasks();
                    break;
                case TerminalCommand.TASK_ASSIGN:
                case TerminalCommand.TA:
                    assignTaskToProject();
                    break;
                case TerminalCommand.TASK_UPDATE:
                case TerminalCommand.TU:
                    updateTask();
                    break;
                case TerminalCommand.TASK_REMOVE:
                case TerminalCommand.TR:
                    removeTaskByName();
                    break;
                case TerminalCommand.TASK_CLEAR:
                case TerminalCommand.TCL:
                    clearTasks();
                    break;
                case TerminalCommand.EXIT:
                case TerminalCommand.E:
                    System.exit(0);
                    break;
            }
        }
    }
}
