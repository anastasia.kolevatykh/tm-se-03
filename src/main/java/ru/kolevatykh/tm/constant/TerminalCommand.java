package ru.kolevatykh.tm.constant;

public class TerminalCommand {
    public static final String HELP = "help";
    public static final String H = "h";
    public static final String PROJECT_CREATE = "project-create";
    public static final String PCR = "pcr";
    public static final String PROJECT_LIST = "project-list";
    public static final String PL = "pl";
    public static final String PROJECT_SHOW = "project-show";
    public static final String PSH = "psh";
    public static final String PROJECT_UPDATE = "project-update";
    public static final String PU = "pu";
    public static final String PROJECT_CLEAR = "project-clear";
    public static final String PCL = "pcl";
    public static final String PROJECT_REMOVE = "project-remove";
    public static final String PR = "pr";
    public static final String TASK_CREATE = "task-create";
    public static final String TCR = "tcr";
    public static final String TASK_LIST = "task-list";
    public static final String TL = "tl";
    public static final String TASK_ASSIGN = "task-assign";
    public static final String TA = "ta";
    public static final String TASK_UPDATE = "task-update";
    public static final String TU = "tu";
    public static final String TASK_CLEAR = "task-clear";
    public static final String TCL = "tcl";
    public static final String TASK_REMOVE = "task-remove";
    public static final String TR = "tr";
    public static final String EXIT = "exit";
    public static final String E = "e";
}
