package ru.kolevatykh.tm.service;

import ru.kolevatykh.tm.entity.Project;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class ProjectService {
    private List<Project> projects = new ArrayList<>();

    public void create(String projectName, String projectDescription, Date startDate, Date endDate) {
        Project project = new Project();
        project.setName(projectName);
        project.setDescription(projectDescription);
        project.setStartDate(startDate);
        project.setEndDate(endDate);
        projects.add(project);
    }

    public Project findByName(String name) {
        for (Project p : projects) {
            if (name.equals(p.getName())) {
                return p;
            }
        }
        return null;
    }

    public void readAll() {
        StringBuilder stringBuilder = new StringBuilder();
        int i = 0;

        for (Project project : projects) {
            stringBuilder
                    .append(++i)
                    .append(". ")
                    .append(project.toString())
                    .append(System.lineSeparator());
        }

        System.out.println(stringBuilder);
    }

    public void update(String projectId, String projectName, String projectDescription) {
        for (Project project : projects) {
            if (projectId.equals(project.getId())) {
                project.setName(projectName);
                project.setDescription(projectDescription);
            }
        }
    }

    public void clearAll() {
        projects = new ArrayList<>();
    }

    public void removeByName(String name) {
        Iterator<Project> it = projects.iterator();
        while (it.hasNext()) {
            Project project = it.next();
            if (name.equals(project.getName())) {
                it.remove();
            }
        }
    }
}
