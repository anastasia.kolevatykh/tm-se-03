package ru.kolevatykh.tm.service;

import ru.kolevatykh.tm.entity.Task;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class TaskService {
    private List<Task> tasks = new ArrayList<>();

    public void create(String taskName, String taskDescription, Date startDate, Date endDate) {
        Task task = new Task();
        task.setName(taskName);
        task.setDescription(taskDescription);
        task.setStartDate(startDate);
        task.setEndDate(endDate);
        tasks.add(task);
    }

    public Task findByName(String name) {
        for (Task task : tasks) {
            if (name.equals(task.getName())) {
                return task;
            }
        }
        return null;
    }

    public void readByProjectId(String projectId) {
        StringBuilder projectTasks = new StringBuilder();
        int i = 0;

        for (Task task : tasks) {
            if (projectId.equals(task.getProjectId())) {
                projectTasks
                        .append(++i)
                        .append(". ")
                        .append(task.toString())
                        .append(System.lineSeparator());
            }
        }

        System.out.println(projectTasks);
    }

    public void readAll() {
        StringBuilder allTasks = new StringBuilder();
        int i = 0;

        for (Task task : tasks) {
            allTasks
                    .append(++i)
                    .append(". ")
                    .append(task.toString())
                    .append(System.lineSeparator());
        }

        System.out.println(allTasks);
    }

    public void assignToProject(String taskId, String projectId) {
        for (Task task : tasks) {
            if (taskId.equals(task.getId())) {
                task.setProjectId(projectId);
            }
        }
    }

    public void update(String taskId, String taskName, String taskDescription) {
        for (Task task : tasks) {
            if (taskId.equals(task.getId())) {
                task.setName(taskName);
                task.setDescription(taskDescription);
            }
        }
    }

    public void clearAll() {
        tasks = new ArrayList<>();
    }

    public void removeByName(String projectId, String taskName) {
        Iterator<Task> it = tasks.iterator();
        while (it.hasNext()) {
            Task task = it.next();
            if (taskName.equals(task.getName())
                    && (projectId.equals(task.getProjectId()))) {
                it.remove();
            }
        }
    }

    public void removeByProjectId(String projectId) {
        Iterator<Task> it = tasks.iterator();
        while (it.hasNext()) {
            Task task = it.next();
            if (projectId.equals(task.getProjectId())) {
                it.remove();
            }
        }
    }
}
